package com.example.user.mytabs.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.user.mytabs.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        bLogin.setOnClickListener {
            loginClickAction()
        }

        tSignup.setOnClickListener {
            var intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

    }

    fun loginClickAction() {
        eEmail.error = null
        ePassword.error = null

        Toast.makeText(this, "Button pressed", Toast.LENGTH_SHORT).show()
        Toast.makeText(this, validation().toString(), Toast.LENGTH_SHORT).show()


        if (validation()) {

            var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

            Toast.makeText(this, "VALID", Toast.LENGTH_SHORT).show()

            if(mAuth.currentUser != null) {
                mAuth.signOut()
                Toast.makeText(this, "THERE WAS A USER", Toast.LENGTH_SHORT).show()
            }

            mAuth.signInWithEmailAndPassword(eEmail.text.toString(), ePassword.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "SUCCESSFULL pressed", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, eEmail.text.toString(), Toast.LENGTH_SHORT).show()
                        var intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this, "Not Successful", Toast.LENGTH_SHORT).show()

                    }
                }.addOnFailureListener { task->
                    Toast.makeText(this, task.message, Toast.LENGTH_SHORT).show()
                }.addOnSuccessListener {
                    task ->
                    Toast.makeText(this, "OnSuccess pressed", Toast.LENGTH_SHORT).show()
                }
        }
    }


        fun validation(): Boolean {

            var email = eEmail.text.toString()
            var pass = ePassword.text.toString()
            var validation: Boolean = true

            if (email.isEmpty()) {
                eEmail.error = "Please check your e-mail"
                validation = false
            }
            if (pass.isEmpty()) {
                ePassword.error = "Please check your password"
                validation = false
            }
            return validation
        }
}
