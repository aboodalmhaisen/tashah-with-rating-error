package com.example.user.mytabs.Activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.user.mytabs.R
import kotlinx.android.synthetic.main.profile_activity.*

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_activity)

        mSettingButton.setOnClickListener {
            val intent= Intent(this,SittingsActivity::class.java)
            startActivity(intent)
  }
 }
}