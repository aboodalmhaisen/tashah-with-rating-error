package com.example.user.mytabs.Activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.user.mytabs.Adapters.SettingAdapter
import com.example.user.mytabs.Item.SettingsItem
import com.example.user.mytabs.R
import kotlinx.android.synthetic.main.settings.*

class SittingsActivity :AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings)


        var Settings = ArrayList<SettingsItem>()

        Settings.add(
            SettingsItem(
                "Account Info")

        )
        Settings.add(
            SettingsItem(
                "Change Email")

        )
        Settings.add(
            SettingsItem(
                "Change Password")

        )
        Settings.add(
            SettingsItem(
                "Language")

        )
        Settings.add(
            SettingsItem(
                "SignOut")

        )

        mListView.adapter = SettingAdapter(applicationContext,Settings)




    }
}