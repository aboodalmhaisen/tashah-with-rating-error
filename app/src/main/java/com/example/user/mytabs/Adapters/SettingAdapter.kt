package com.example.user.mytabs.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.user.mytabs.Item.SettingsItem
import com.example.user.mytabs.R
import kotlinx.android.synthetic.main.settings_list.view.*

class SettingAdapter(var context: Context, var Settings: ArrayList<SettingsItem>) : BaseAdapter(){
    override   fun getItem(position: Int): SettingsItem {
        return Settings.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return Settings.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        var mainView: View
        var viewHolder: ViewHolder
        if (convertView == null) {

            var layoutInflater = LayoutInflater.from(context);
            mainView = layoutInflater.inflate(R.layout.settings_list, parent, false)
            viewHolder = ViewHolder(mainView.SettingName)
            Log.d("MYAPP", "We are here for " + position)

            mainView.tag = viewHolder
        } else {
            mainView = convertView
            viewHolder = convertView.tag as ViewHolder
        }


        var Settings: SettingsItem = getItem(position)
        viewHolder.mTextViewFullName.text = Settings.SettingName

        //Picasso.get().load(profile.profilePicture).into(viewHolder.mImageView);

        return mainView
    }

    private class ViewHolder(var mTextViewFullName: TextView)
}


