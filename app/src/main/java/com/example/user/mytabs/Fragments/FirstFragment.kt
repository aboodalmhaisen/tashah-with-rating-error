package com.example.user.mytabs.Fragments


import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.example.user.imageslider.TopFiveSlidingAdapter
import com.example.user.mytabs.Models.ImageModel
import com.example.user.mytabs.R
import kotlinx.android.synthetic.main.fragment_first.*
import kotlinx.android.synthetic.main.fragment_first.view.*
import java.util.*


/**
    When doing things in a clean matter we:
        Devide everything in folders ( Packages )
        We Create a name full variables
        we add comments on every function.

        Sorry el keyboard sharat
        el mohem see how we structuer things yes
        and how can i copy and paste what i have done in the other projects here ?
        take a look


 */

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 *
 */
class FirstFragment : Fragment() {

    private var imageModelArrayList: ArrayList<ImageModel>? = null

    private val myImageList = arrayListOf<String>(
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "http://www.realdetroitweekly.com/wp-content/uploads/2017/06/Restaurants.jpg",
        "https://static.standard.co.uk/s3fs-public/thumbnails/image/2016/06/14/06/the-clove-club.jpg?w968"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


//        See the fragment is empty
//        First of all we will add the ViewPager dependency
//        then we will create a model class for the images
//        then a adapter
//        and then link them together
//        Okay ?

        var myView = inflater.inflate(R.layout.fragment_first, container, false)

        imageModelArrayList = ArrayList()
        imageModelArrayList = populateList()

        myView.mViewPager.adapter = TopFiveSlidingAdapter(activity!!.applicationContext, this.imageModelArrayList!!)

        myView.mIndicator.setViewPager(myView.mViewPager)
        val density = resources.displayMetrics.density

        myView.mIndicator.setRadius(5 * density)
        var NUM_PAGES = imageModelArrayList!!.size
        var currentPage: Int = 0
        // Auto start of viewpager
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            myView.mViewPager!!.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 5000, 5000)

        // Pager listener over indicator
        myView.mIndicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {
                if (currentPage == myImageList.size) {
                    currentPage = 0
                    myView.mViewPager.setCurrentItem(currentPage, true)
                }
            }

            override fun onPageScrollStateChanged(pos: Int) {
            }

//            override fun onTouchEvent(event: MotionEvent):Boolean{
//                if(this.enabled)
//
////                    Okay hasa 2nty el mafrood ma t3maleh hoon look
//            }
        })


        // Inflate the layout for this fragment
        return myView
    }

    private fun populateList(): ArrayList<ImageModel> {

        val list = ArrayList<ImageModel>()

        for (image in myImageList) {
            val imageModel = ImageModel(image)
            list.add(imageModel)
        }
        return list
    }

    private fun init() {


    }


}
