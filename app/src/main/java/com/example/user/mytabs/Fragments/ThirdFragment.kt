package com.example.user.mytabs.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.user.mytabs.Models.Place
import com.example.user.mytabs.Adapters.PlacesAdapter
import com.example.user.mytabs.R
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import kotlinx.android.synthetic.main.fragment_third.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ThirdFragment : Fragment() {
    var firebaseStore: FirebaseFirestore = FirebaseFirestore.getInstance()
    var places = ArrayList<Place>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    )
            : View? {
        // Inflate the layout for this fragment
        var mView = inflater.inflate(R.layout.fragment_third, container, false)

        val settings = FirebaseFirestoreSettings.Builder()
            .setTimestampsInSnapshotsEnabled(true).build()
        firebaseStore.setFirestoreSettings(settings)
        getPlaces()
        mView.mListView.adapter = PlacesAdapter(activity!!.applicationContext, places)
        return mView
    }

    init {
    }

    fun getPlaces(){
        firebaseStore.collection("places").get().addOnSuccessListener {documents->

            documents.forEach{document->
                var name: String = document.data.get("Name") as String
                var location: String = document.data.get("Location") as String
                var description: String = document.data.get("Description") as String
                var rating: Float = document.data.get("Rating") as Float
                var image: String = document.data.get("Image") as String

                places.add(
                    Place(
                        name,
                        description,
                        location,
                        rating,
                        image

                    )
                )

            }

        }

    }
}
